/**
 * Service endpoint for newsHack returning JSON data
 */
var restify = require('restify');
var http = require('http');
var querystring = require('querystring');
var Twitter = require('twitter');
var moment = require('moment');

var ip_addr = '127.0.0.1';
var port    =  '8897';




var perspectives = {
                    left: {title: "Left", order: 1, sources: [12,8,11]},
                    neutral: {title: "Neutral", order: 2, sources: [1,10]},
                    right: {title: "Right", order: 3, sources: [42, 43,45]},
                    international: {title: "Interational", order: 6},
                    national: {title: "National", order: 5},
                    regional: {title: "Regional", order: 4},		
};


var stories = {
	'House Prices' : [{id: "fec3f4d703082dfea3aa078eb4cbee35019b9363", perspective: "left"},
    	             {id: "c774632e80703cf438a5f9b3b7bda079d2e34bce",  perspective: "neutral"},
          	         {id: "ba715face36a6ce56e093f82614a01804cd5067e",  perspective: "right"}],
    'EU Referendum' : [{id: "d267533ae7da764a8e0defe8d84a76606d5ef5a1", perspective: "left"},
        	         {id: "9aa558c7b93ed06d6f86ee5ccf40e1e791e42b7b", perspective: "neutral"},
          	         {id: "813a60db778a844d989ef73c8a57e9ff905d0a76", perspective: "right"}],
   	'Immigration' : [{id: "2d04402571b33af55b5eba296daa9a7c2cd52afb", perspective: "left"},
    	             {id: "695c985d22dbd1d7e3636f97dbd220fa4492d567", perspective: "neutral"},
          	         {id: "244865b0dc499cb52ff9a7c9a4d3c7353fdede67", perspective: "right"}],
    'Austerity' :   [{id: "b03001096b80522dddb1456f84aa0000f68b7394", perspective: "left"},
        	         {id: "2afbe3d4c58bb1576d5343321649cd80a0308745", perspective: "neutral"},
          	         {id: "27f4c3c0ea56503b5b50837570a08d5e364b2896", perspective: "right"}]	
}

var topTopics = { topics : [
                   	     {title: 'House Prices',
                   	      description: 'House Prices',
                   	      storiesUrl: '/topicStories/House%20Prices',
                   	      imageUrl: "house-prices.jpg",
                   	      sources: [{publication: "The Mirror", perspective: "Left"}, {publication: "BBC News", perspective: "Neutral"},{publication: "Daily Express", perspective: "Right"}]},
                   	     {title: 'EU Referendum',
                   	      description: "Should we stay in the EU?",
                   	      storiesUrl: '/topicStories/EU Referendum',
                   	      imageUrl: "eureferendum.jpg",
                       	  sources: [{publication: "The Guardian", perspective: "Left"}, {publication: "BBC News", perspective: "Neutral"},{publication: "Daily Express", perspective: "Right"}]},
                   	     {title: 'Immigration',
                   	      description: 'Is immigration good for the UK?',
                   	      storiesUrl: '/topicStories/Immigration',
                   	      imageUrl: "immigration.jpg",
                       	  sources: [{publication: "The Huffington Post", perspective: "Left"}, {publication: "The Independent", perspective: "Neutral"},{publication: "Daily Mail", perspective: "Right"}]},
                   	     {title: 'Austerity',
                   	      description: 'Is austerity good or bad?',
                   	      storiesUrl: '/topicStories/Austerity',
                   	      imageUrl: "austerity.jpg",
                   	      sources: [{publication: "The Mirror", perspective: "Left"}, {publication: "BBC News", perspective: "Neutral"},{publication: "The Daily Telegraph", perspective: "Right"}]}
                   	    ]};




// http://data.test.bbc.co.uk/bbcrd-juicer/articles?q=%22Mansion%20Tax%22&sources[]=58&sources[]=58&apikey=3O320TNQSzygKXF8frRiNBQnAANSyUl7&size=1000
var API_KEY = "apikey=3O320TNQSzygKXF8frRiNBQnAANSyUl7"
var ARTICLES_ENDPOINT = "/bbcrd-juicer/articles"
var JUICER_SERVER_URL = 'http://data.test.bbc.co.uk'
	
var juicerClient = restify.createJsonClient(JUICER_SERVER_URL);
	
/**
 * Use Restify to build RESTful service endpoint
 */
var server = restify.createServer({
  name : "NewsHack API"
});

server.use(restify.queryParser());
server.use(restify.bodyParser({ mapParams: false }));
server.use(restify.CORS());
server.listen(port ,ip_addr, function(){
  console.log('%s listening at %s ', server.name , server.url);
});


/**
 * 
 * @param query
 * @param sources
 * @returns
 */
function findStories(query, sources, callback){
	
	// http://data.test.bbc.co.uk/bbcrd-juicer/articles?q=kenya%20OR%20nairobi%20AND%20(government%20OR%20%22Uhuru%20Kenyatta%22)&apikey={apikey}
	var stories = {};
	
	var sourcesParams = "";
	if (sources){
		sources.forEach(function(sourceNum){
			sourcesParams += "&sources[]=" + sourceNum
		}
	);
	}
		
	var query = ARTICLES_ENDPOINT + "?q=" + query + sourcesParams + "&size=10" + "&" + API_KEY;
	
	console.log("query: " + query);
	
    juicerClient.get(query, function(jerr, jreq, jres, jobj) {
        console.log("In juicer client get");
        
        if (jerr){
        	console.log(jerr);
        	callBack ( jerr, stories)
        } else {
        //console.log('%j', jobj);   
        stories = jobj;
        callback( null, stories);
        }
      });
}


/**
 * 
 * @param id
 * @param callback
 */
function getJuicerArticle(id, callback){	
	
	var query = ARTICLES_ENDPOINT + "/" + id + "?" + API_KEY;
	
	console.log("query: " + query);
	
    juicerClient.get(query, function(jerr, jreq, jres, jobj) {
        console.log("In juicer client get");
        
        if (jerr){
        	console.log(jerr);
        	callBack ( jerr, jobj)
        } else {
        //console.log('%j', jobj);       
        callback( null, jobj);
        }
      });
	
}


/**
 * 
 * @param req
 * @param res
 * @param next
 */
function getArticle(req, res, next){
	
	var query = ARTICLES_ENDPOINT + "/" + req.params[0] + "?" + API_KEY;
	console.log("query: " + query);

	getJuicerArticle(req.params[0], function(err, obj) {

		if (!err){
			res.send(200, obj);
		} else {
			res.send(404, "Article Not Found")
		}
	});
	
}



/**
 * 
 * @param req
 * @param res
 * @param next
 */
function getMatchingStories(req, res, next){
	
	
	findStories(req.params[0], [12,58], function(err, obj) {

		if (!err){
			res.send(200, obj);
		} else {
			res.send(404, "Articles Not Found")
		}
	});	
}



function getTopics(req, res, next){
	
	var result = topTopics;
	
	res.send(200, result);
	next();
}



function getTopic(req, res, next){
	
	var result = { topic : req.params[0]};
	
	
	//JSON.stringify(result, null, 2);
    res.setHeader("Content-Type", "application/json");
	res.send(200, result);
	next();
	
}

function getStoryPerspective(id){
	
	var perspective = "";
	
	for(var topic in stories) { 
		   if (stories.hasOwnProperty(topic)) {
	       
		       stories[topic].forEach(function(st){
		    	   
		    	   //console.log("st.id is " + st.id + " id is " + id);
		    	   if (st.id == id){
		    		   perspective = st.perspective;
		    		   		    	   }
		       });		       
		   }
		}
	
	return perspective;
}


/**
 * 
 * @param topic
 * @param callback
 */
function getPredefinedStories(topic, callback){
		
	var result = [];
	var numRequests = 0;

	stories[topic].forEach(function(st){
		
		console.log("Getting id: " + st.id);
		getJuicerArticle(st.id, function(err, obj) {

			if (!err){
				console.log("Id: " + obj.id + " has perspective " + getStoryPerspective(obj.id));
				obj['tmPerspective'] = getStoryPerspective(obj.id); //st.perspective;
				obj.tmDate = moment(obj.published).format('Do MMM YYYY');
				result.push(obj);
			} else {
				Console.log("Article Not Found");
				callback(err, result);
			}
			
			numRequests++;
			
			if (numRequests == stories[topic].length){
				// All done
				
				// Sort to desired order
				
				result.sort(function(a,b) { return perspectives[a.tmPerspective].order - perspectives[b.tmPerspective].order } );
				//JSON.stringify(result, null, 2);
				callback(null, result);
			}
		});
	});

}


/**
 * 
 * @param topic
 * @param callback
 */
function topicStoriesSearch(topic, callback){
	
	
	var sources = [];
	
    // var random = items[Math.floor(Math.random()*items.length)]
	
}



/**
 * 
 * @param req
 * @param res
 * @param next
 */
function getTopicStories(req, res, next){
	
	var result = {};
	
	var topic = querystring.unescape(req.params[0]);
	
	
	if (stories.hasOwnProperty(topic)){
		
		result.topic = topic;
		
		console.log("Getting stories for predefined topic: " + stories[topic]);
		 
		getPredefinedStories(topic, function(err, obj){		
			if (!err){
				
			result.data = obj;
		    res.setHeader("Content-Type", "application/json");
			res.send(200, result);
			next();
			} else {
				res.send(404, result);
			}
		});
		
//		var numRequests = 0;
//
//		stories[topic].forEach(function(st){
//			
//			console.log("Getting id: " + st.id);
//			getJuicerArticle(st.id, function(err, obj) {
//
//				if (!err){
//					console.log("Id: " + obj.id + " has perspective " + getStoryPerspective(obj.id));
//					obj['tmPerspective'] = getStoryPerspective(obj.id); //st.perspective;
//					result.push(obj);
//				} else {
//					Console.log("Article Not Found");
//				}
//				
//				numRequests++;
//				
//				if (numRequests == stories[topic].length){
//					// All done
//					
//					// Sort to desired order
//					
//					result.sort(function(a,b) { return perspectives[a.tmPerspective].order - perspectives[b.tmPerspective].order } );
//					//JSON.stringify(result, null, 2);
//				    res.setHeader("Content-Type", "application/json");
//					res.send(200, result);
//					next();
//				}
//			});
//		});
//		
		
	} else {
		
		// Search for topic stories
		
		
		console.log("Can't find topic: " + topic);
	    res.setHeader("Content-Type", "application/json");
		res.send(404, "Not Found");
		next();
	}	
}


/**
 * Searches for tweets on a topic
 * 
 * @param req
 * @param res
 * @param next
 */
function getTweets(req, res, next){
	
	 
	var client = new Twitter({
	  consumer_key: 'xxx',
	  consumer_secret: 'xxx',
	  access_token_key: 'xxx',
	  access_token_secret: 'xxx'
	});
	 
	var params = {q: querystring.unescape(req.params[0])};
	client.get('search/tweets', params, function(error, tweets, response){
	  if (!error) {
	    console.log(tweets);
	    res.setHeader("Content-Type", "application/json");
		res.send(200, tweets);
		next();
	  } else {
			console.log("Can't find tweets: " + querystring.unescape(req.params[0]));
		    res.setHeader("Content-Type", "application/json");
			res.send(404, "Not Found");
			next();		  
	  }
	});
	
}



//Service endpoints
server.get({path : /^\/topics$/ , version : '0.0.1'} , getTopics);
server.get({path : /^\/topic\/(.*)$/ , version : '0.0.1'} , getTopic);
server.get({path : /^\/topicStories\/(.*)$/ , version : '0.0.1'} , getTopicStories);
server.get({path : /^\/article\/(.*)$/ , version : '0.0.1'} , getArticle);
server.get({path : /^\/find\/(.*)$/ , version : '0.0.1'} , getMatchingStories);
server.get({path : /^\/tweets\/(.*)$/ , version : '0.0.1'} , getTweets);


